import kivy
# kivy.require('1.0.7')

from kivy.app import App
from kivy.uix.button import Button
from kivy.uix.dropdown import DropDown
from kivy.uix.gridlayout import GridLayout 
from kivy.uix.label import Label
from kivy.uix.textinput import TextInput

from kivy.uix.behaviors import ButtonBehavior
from recharge import recharge


class MyButton(Button):
    def __init__(self, **kwargs):
        super(MyButton, self).__init__(**kwargs)

    def on_press(self):
        print("Button Pressed")

    def callback(instance):
        print('The button <%s> is being pressed' % instance)


class LoginScreen(GridLayout):

    def __init__(self, **kwargs): 
        super(LoginScreen, self).__init__(**kwargs) 
        self.cols = 2 
        # self.size_hint=(20, 30)
        self.add_widget(Label(text='PIN')) 
        self.pin = TextInput(multiline=False)
        self.add_widget(self.pin)

        self.add_widget(Label(text='PORT')) 
        self.port = TextInput(multiline=False,text="COM")
        self.add_widget(self.port)

        self.add_widget(Label(text='LOCATION OF CSV')) 
        self.csv = TextInput(multiline=False,text=r"C:\Users\Finder\Documents\recharge\2017-08-26.csv")
        self.add_widget(self.csv)
        print("DIR OF TEXT INPUT",self.csv.text)
        
        self.add_widget(Label(text='LOCATION TO SAVE IN SMS CSV')) 
        self.csv2 = TextInput(multiline=False,text=r"C:\Users\Finder\Documents\New folder\sms.txt")
        self.add_widget(self.csv2)

        def callback(instance):
            print('The button <%s> is being pressed' % instance.text)
        
            print('The data is %s' % self.csv.text)
            recharge(file=self.csv.text,pin=self.pin.text,file_out=self.csv2.text,port=self.port.text,baudrate=9600)

        #self.instance = {}
        #self.text = "WOW DATA"
        #instance.text = "WOW DATA"
        self.add_widget(Label(text='')) 
        self.submit_button = MyButton(text="Start Loading")
        # print("SELF is ")
        # print(self)
        self.submit_button.bind(on_press=callback)
        self.add_widget(self.submit_button)

        

class Finder_SMSApp(App):
    def build(self):
        return LoginScreen()
#     def build(self):
#         # return Button(text='Hello World')
#         # dropdown = CustomDropDown()
#         # print("Testing CustomeDropDown",dropdown)
#         # mainbutton = Button(text='Hello', size_hint=(None, None))
#         # mainbutton.bind(on_release=dropdown.open)
#         # dropdown.bind(on_select=lambda instance, x: setattr(mainbutton, 'text', x))



if __name__ == '__main__':
    Finder_SMSApp().run()