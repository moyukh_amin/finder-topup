#!/usr/bin/env python
#sudo apt-get install python-serial
#Postpaid/Prepaid Recharge
import serial
import time
import csv
import requests
import re

def sendAT(ser,cmd):
    ser.write(cmd.encode())
    msg = ser.read(1000)
    return str(msg)
    #time.sleep(1)
    
def readsms(port,file):
    #sendAT(ser,"ATZ\r\n")
    ser = serial.Serial(port=port, baudrate=9600, timeout=10)
    sendAT(ser,"AT\r\n")
    sendAT(ser,"AT\r\n")
    sendAT(ser,"AT+CMGF=1\r\n")
    msg = 'AT+CMGL="REC UNREAD"\r\n'
    ser.write(msg.encode())
    msgs = ser.read(1000000)
    print("SMS READ......\n Deleting")
    sendAT(ser, "AT+CMGD=1,1\r\n")
    try:
        with open(file,'a+') as f:
            f.write("\r\r\r\n\n\n\n\n\r\n"+str(msgs))
    except Exception as e:
        print("SMS Error",e)
    ser.close()

def recharge(file,pin,file_out=r'C:\Users\Finder\Downloads\latest script\sms.txt',port="COM7",baudrate=9600):
    try:
        f = open(file)
        csv_f = csv.reader(f)
        ser = serial.Serial(port=port, baudrate=9600, timeout=1.5)
        i=1
        unsuccesful = []
        for row in csv_f:
            tartTime = time.time()
            mobile_number = str(row[0])
            recharge_amount = str(row[2])
            sim_type = str(row[1])
            msg ={"phone":mobile_number,"recharge_amount":recharge_amount,"secret_key":"DC1731A993DF96FAB1D1EAB6D4334"}
            requests.post("http://mint.finder-lbs.com/api/v1/recharge/number", data=msg)
            print ("Starting recharge for ",mobile_number+" , of  "+recharge_amount+"Tk")
            try:
                cmds = []
                cmds.append('AT\r\n')
                cmds.append('AT*PSSTKI=1\r\n')
                cmds.append('AT*PSSTK="SETUP MENU",1,1\r\n')
                cmds.append('AT*PSSTK="GET ITEM LIST",8\r\n') ##Prepaid Topup
                cmds.append('AT*PSSTK="MENU SELECTION",128\r\n') ##Select item
                cmds.append('AT*PSSTK="GET ITEM LIST",10\r\n') ##View available options.
                cmds.append('AT*PSSTK="SELECT ITEM",128,'+sim_type+',0,0\r\n') ## Select prepaid/postpaid option
                cmds.append('AT*PSSTK="GET INPUT",4,0,"'+ mobile_number +'",0,0,\r\n') ##Customer Mobile No.
                cmds.append('AT*PSSTK="GET INPUT",4,0,"'+ mobile_number +'",0,0,\r\n') ##Confirm Mobile No.
                cmds.append('AT*PSSTK="GET INPUT",4,0,"'+ recharge_amount +'",0,0,\r\n') ##Recharge Amount
                cmds.append('AT*PSSTK="GET INPUT",4,0,"'+ recharge_amount +'",0,0,\r\n') ##Confirm Amount
                cmds.append('AT*PSSTK="GET INPUT",4,1,"'+ pin +'",0,0,\r\n') ##PIN
                cmds.append('AT*PSSTK="DISPLAY TEXT",1,0\r\n') ##DISPLAY TEXT
                cmds.append(' AT*PSSTK="NOTIFICATION",19,1\r\n') ## NOTIFICATION
                cmds.append(' AT*PSSTK="NOTIFICATION",19,1\r\n')
                j = 0
                for cmd in cmds:
                    msg = sendAT(ser,cmd)
                    print(msg)
                    if re.search(r'ERROR',str(msg)) or msg=="b''":
                        j+=1
                if j>5:
                    msg = {"phone":mobile_number,"recharge_amount":recharge_amount,"status":"Failed","secret_key":"DC1731A993DF96FAB1D1EAB6D4334"}
                    print("Recharge Unsuccesfull For"+mobile_number,"Too soon")
                    unsuccesful.append(mobile_number)
                else:
                    msg = {"phone":mobile_number,"recharge_amount":recharge_amount,"status":"Waiting","secret_key":"DC1731A993DF96FAB1D1EAB6D4334"}
                    print("Recharge Succesfull",mobile_number,"For ",recharge_amount)                    
                requests.post("http://mint.finder-lbs.com/api/v1/recharge/attempt", data=msg)
                print("Request posted")
            except Exception as e:
                ser.close()
                print("Recharge Unsuccesfull For"+mobile_number,e)
                try:
                    ser = serial.Serial(port=port, baudrate=9600, timeout=2)
                except Exception as e:
                    print("Modem Error",e)
            if i%20==0:
                ser.close()
                print("Reading SMS")
                readsms(port,file_out)
                ser = serial.Serial(port=port, baudrate=9600, timeout=2)
                i+=1
            else:
                i+=1
            print ('Recharge took {0} second !'.format(time.time() - tartTime))
        ser.close()
        print(unsuccesful)
        print("STARTING to read SMS")
        time.sleep(100)
        readsms(port,file_out)
    except Exception as e:
        ser.close()
        print("Error: ",e)

