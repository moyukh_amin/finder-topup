import re
import os
import time
import sqlite3
from datetime import datetime

def create_sms_table():
    try:
        conn = sqlite3.connect(os.path.expanduser('~')+r'\Documents\finder.db')
        print("Connected")
    except Exception as e:
        print("Error connecting to DB", e)
    try:
        conn.execute('''CREATE TABLE SMS
             (ID INTEGER PRIMARY KEY   AUTOINCREMENT NOT NULL,
             MSG  CHAR(255), 
             SENT_FLAG  BOOLEAN  NOT NULL,
             TIME_OF_DATA DATETIME);''')
        print("Creating SMS Table")
        conn.commit()
        conn.close()
    except Exception as e:
        print("Error Creating Table", e)

def insert_sms_commands_table(msg,sent_flag,time_of_data = datetime.now()):
    '''CREATE TABLE SMS_COMMANDS
           (ID INTEGER PRIMARY KEY   AUTOINCREMENT NOT NULL,
           SENDER  CHAR(50),
           MSG  CHAR(255),
           EXECUTED  BOOLEAN  NOT NULL,
           TIME_OF_DATA DATETIME);'''
    try:
        conn = sqlite3.connect(os.path.expanduser('~') + r'\Documents\finder.db')
    except Exception as e:
        print("Error connecting to DB",e)
    try:
        conn.execute("INSERT INTO SMS_COMMANDS (MSG,SENT_FLAG,TIME_OF_DATA) \
      VALUES (?,?,?)",(msg,sent_flag,time_of_data));
        conn.commit()
        conn.close()
    except Exception as e:
        print("Error inserting to SMS_COMMANDS",e)

def send_sms_to_database(file = r'\Desktop\sms.txt')
    while True:
        try:
            smss = []
            with open(os.path.expanduser('~') + str(file),'r+') as file:
                for line in file:
                    smss.append(line)
                file.truncate()
                file.close()
                for sms in smss:
                    if "CMGL" in sms:
                        msg = str(sms).split("CMGL")
                        insert_sms_commands_table(msg,0)
        except Exception as e:
            print("Error While Inserting To DATABASE:  ",e)