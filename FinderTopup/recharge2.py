#!/usr/bin/env python
# sudo apt-get install python-serial
# Postpaid/Prepaid Recharge


import serial
import time
import csv
import requests
import re

def sendAT(ser, cmd):
    ser.write(cmd.encode())
    msg = ser.read(1000)
    return str(msg)


def readsms(port,baudrate,file):
    # sendAT(ser,"ATZ\r\n")
    print("Reading SMS")
    ser = serial.Serial(port=port, baudrate=baudrate, timeout=10)
    sendAT(ser, "AT\r\n")
    sendAT(ser, "AT\r\n")
    sendAT(ser, "AT+CMGF=1\r\n")
    msg = 'AT+CMGL="REC UNREAD"\r\n'
    ser.write(msg.encode())
    msgs = ser.read(1000000)
    print("SMS READ.....\nDeleting.....")
    sendAT(ser, "AT+CMGD=1,1\r\n")
    try:
        with open(file, 'a+') as f:
            f.write(str(msgs))
    except Exception as e:
        msg = {"error": "SMS Error"+str(e),
               "secret_key": "DC1731A993DF96FAB1D1EAB6D4334"}
        requests.post("http://mint.finder-lbs.com/api/v1/recharge/error", data=msg)
        print("SMS Write Error", e)
    ser.close()


def recharge(mobile_number,sim_type,recharge_amount,pin,port,baudrate):
    try:
        ser = serial.Serial(port=port, baudrate=baudrate, timeout=1.5)
        if check_modem(ser):
            print("Modem is OK")
        else:
            msg = {"error": "Module is not Responding",
                   "secret_key": "DC1731A993DF96FAB1D1EAB6D4334"}
            requests.post("http://mint.finder-lbs.com/api/v1/recharge/error", data=msg)
            return
        msg = {"phone": mobile_number, "status": "Pending",
               "secret_key": "DC1731A993DF96FAB1D1EAB6D4334"}
        requests.post("http://mint.finder-lbs.com/api/v1/recharge/pending", data=msg)
        print("Starting recharge for ", mobile_number + " , of  " + recharge_amount + "Tk")
        try:
            cmds = []
            cmds.append('AT\r\n')
            cmds.append('AT*PSSTKI=1\r\n')
            cmds.append('AT*PSSTK="SETUP MENU",1,1\r\n')
            cmds.append('AT*PSSTK="GET ITEM LIST",8\r\n')  ##Prepaid Topup
            cmds.append('AT*PSSTK="MENU SELECTION",128\r\n')  ##Select item
            cmds.append('AT*PSSTK="GET ITEM LIST",10\r\n')  ##View available options.
            cmds.append('AT*PSSTK="SELECT ITEM",128,' + sim_type + ',0,0\r\n')  ## Select prepaid/postpaid option
            cmds.append('AT*PSSTK="GET INPUT",4,0,"' + mobile_number + '",0,0,\r\n')  ##Customer Mobile No.
            cmds.append('AT*PSSTK="GET INPUT",4,0,"' + mobile_number + '",0,0,\r\n')  ##Confirm Mobile No.
            cmds.append('AT*PSSTK="GET INPUT",4,0,"' + recharge_amount + '",0,0,\r\n')  ##Recharge Amount
            cmds.append('AT*PSSTK="GET INPUT",4,0,"' + recharge_amount + '",0,0,\r\n')  ##Confirm Amount
            cmds.append('AT*PSSTK="GET INPUT",4,1,"' + pin + '",0,0,\r\n')  ##PIN
            cmds.append('AT*PSSTK="DISPLAY TEXT",1,0\r\n')  ##DISPLAY TEXT
            cmds.append(' AT*PSSTK="NOTIFICATION",19,1\r\n')  ## NOTIFICATION
            cmds.append(' AT*PSSTK="NOTIFICATION",19,1\r\n')
            j = 0
            for cmd in cmds:
                msg = sendAT(ser, cmd)
                print(msg)
                if re.search(r'ERROR', str(msg)) or msg == "b''":
                    j += 1
            if j > 5:
                msg = {"phone": mobile_number, "status": "Failed",
                       "secret_key": "DC1731A993DF96FAB1D1EAB6D4334"}
                print("Recharge Unsuccessful For" + mobile_number, "Too soon")
            else:
                msg = {"phone": mobile_number,"status": "Waiting",
                       "secret_key": "DC1731A993DF96FAB1D1EAB6D4334"}
                print("Recharge Successful", mobile_number, "For ", recharge_amount)
            requests.post("http://mint.finder-lbs.com/api/v1/recharge/attempt", data=msg)
            print("Request posted")
        except KeyboardInterrupt:
            raise KeyboardInterrupt
        except Exception as e:
            ser.close()
            msg = {"error": "While Recharging"+str(e),
                   "secret_key": "DC1731A993DF96FAB1D1EAB6D4334"}
            requests.post("http://mint.finder-lbs.com/api/v1/recharge/error", data=msg)
            print("Recharge Unsuccessful For" + mobile_number, e)
        ser.close()
    except KeyboardInterrupt:
        raise KeyboardInterrupt
    except Exception as e:
        ser.close()
        msg = {"error": "After Recharging\n" + str(e),
               "secret_key": "DC1731A993DF96FAB1D1EAB6D4334"}
        requests.post("http://mint.finder-lbs.com/api/v1/recharge/error", data=msg)
        print("Error: ", e)


def check_modem(ser):
    msg = sendAT(ser, "AT\r\n")
    if re.search(r'OK', str(msg)):
        return True
    else:
        return False

def get_number(file=(os.path.expanduser('~') + r'\Desktop\sms.txt'),pin="1231", port="COM7", baudrate=9600):
    i = 0
    try:
        while True:
            msg = {"secret_key":"DC1731A993DF96FAB1D1EAB6D4334"}
            print("Fetching")
            req = requests.post("http://mint.finder-lbs.com/api/v1/recharge/fetch/gp", data=msg)
            received = req.text
            print(received)
            if "number" in received:
                text = received.split(",")
                mobile_number = str(text[1])
                sim_type =  str(text[2])
                recharge_amount = str(text[3])
                recharge(mobile_number,sim_type,recharge_amount,pin,port,baudrate)
            elif "command" in received:
                pass
            elif "Empty" in received:
                time.sleep(120)
            i+=1
            if i%20==0:
                readsms(port,baudrate,file)
    except Exception as e:
        msg = {"error": "Retrieving Number\n" + str(e),
               "secret_key": "DC1731A993DF96FAB1D1EAB6D4334"}
        requests.post("http://mint.finder-lbs.com/api/v1/recharge/error", data=msg)
    finally:
        readsms(port,baudrate,file)

recharge()
readsms("COM7",9600,(os.path.expanduser('~') + r'\Desktop\sms.txt'))