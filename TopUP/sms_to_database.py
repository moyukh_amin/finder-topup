import re
import os
import time
import sqlite3
from datetime import datetime

def create_sms_table():
    try:
        conn = sqlite3.connect(os.path.expanduser('~')+r'\Documents\sms.db')
        print("Connected")
    except Exception as e:
        print("Error connecting to DB", e)
    try:
        conn.execute('''CREATE TABLE SMS
             (ID INTEGER PRIMARY KEY   AUTOINCREMENT NOT NULL,
             MSG  CHAR(2550), 
             SENT_FLAG  BOOLEAN  NOT NULL,
             TIME_OF_DATA DATETIME);''')
        print("Creating SMS Table")
        conn.commit()
        conn.close()
    except Exception as e:
        print("Error Creating Table", e)

def insert_sms_table(msg,sent_flag=0,time_of_data = datetime.now()):
    '''CREATE TABLE SMS_COMMANDS
           (ID INTEGER PRIMARY KEY   AUTOINCREMENT NOT NULL,
           SENDER  CHAR(50),
           MSG  CHAR(255),
           EXECUTED  BOOLEAN  NOT NULL,
           TIME_OF_DATA DATETIME);'''
    try:
        conn = sqlite3.connect(os.path.expanduser('~') + r'\Documents\sms.db')
    except Exception as e:
        print("Error connecting to DB",e)
    try:
        conn.execute("INSERT INTO SMS (MSG,SENT_FLAG,TIME_OF_DATA) \
      VALUES (?,?,?)",(msg,sent_flag,time_of_data))
        conn.commit()
        conn.close()
        print("Inserted into DB")
    except Exception as e:
        print("Error inserting to SMS:  ",e)

def send_sms_to_database(path = (os.path.expanduser('~') + r'\Documents\smss.txt')):
    while True:
        try:
            smss = []
            import os
            if os.path.getsize(path) > 0:
                with open(path,'r+') as file:
                    for line in file:
                        smss.append(line)
                    file.seek(0)
                    file.truncate()
                for sms in smss:
                    if "CMGL" in sms:
                        msgs = str(sms).split("CMGL")
                        for msg in msgs:
                            print(str(msg))
                            insert_sms_table(msg)
            else:
                print("File is Empty")
        except Exception as e:
            print("Error While Inserting To DATABASE:  ",e)
        time.sleep(10)

send_sms_to_database()