import requests
from _datetime import datetime
import time
import sqlite3
import os


def update_sms_table(id, sent_flag):
    try:
        conn = sqlite3.connect(os.path.expanduser('~')+r'\Documents\sms.db')
    except Exception as e:
        print("Error connecting to DB",e)
    try:
        conn.execute("UPDATE SMS SET SENT_FLAG=? WHERE ID=?",(sent_flag,id))
        conn.commit()
        conn.close()
        return True
    except Exception as e:
        print("Error inserting to Table",e)
        return False


def select_last_sms_table():
    try:
        conn = sqlite3.connect(os.path.expanduser('~')+r'\Documents\sms.db')
    except Exception as e:
        print("Error connecting to DB",e)
    data = []
    sms = {}
    try:
        cursor = conn.execute("SELECT ID, MSG FROM SMS WHERE SENT_FLAG=0")
        for row in cursor:
            print("ID = ", row[0])
            print("MSG = ", row[1])
            sms = {
          "id":row[0],
        "msg":row[1]
            }
            data.append(sms)
        conn.close()
    except Exception as e:
        print("Error selecting Database",e)
    return data


def send_to_server():
    while True:
        try:
            print("Checking SMS")
            msgs = select_last_sms_table()
            for msg in msgs:
                s = {"message":str(msg["msg"]),"secret_key": "DC1731A993DF96FAB1D1EAB6D4334"}
                a = requests.post("http://mint.finder-lbs.com/api/v1/recharge/reply", data=s)
                print(a.text)
                if a.status_code==201:
                    update_sms_table(msg["id"],1)
                else:
                    msg = {"error": "Sending SMS to Server  " + str(a.status_code) + a.text + "for the message" + str(msg[msg]),
                           "secret_key": "DC1731A993DF96FAB1D1EAB6D4334"}
                    requests.post("http://mint.finder-lbs.com/api/v1/recharge/error", data=msg)
            time.sleep(2)
        except Exception as e:
            msg = {"error": "Sending SMS to Server  " + str(a.status_code) + a.text + "for the message" + str(msg[msg]),
                   "secret_key": "DC1731A993DF96FAB1D1EAB6D4334"}
            requests.post("http://mint.finder-lbs.com/api/v1/recharge/error", data=msg)


send_to_server()